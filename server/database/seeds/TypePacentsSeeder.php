<?php

use Illuminate\Database\Seeder;
use App\Entities\TypePacent;

class TypePacentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypePacent::create([
            'type_pacent_id' => 1,
            'type_pacent_name' => 'Estudiante'
        ]);

        TypePacent::create([
            'type_pacent_id' => 2,
            'type_pacent_name' => 'Empleado',
        ]);

        TypePacent::create([
            'type_pacent_id' => 3,
            'type_pacent_name' => 'Visita',
        ]);
    }
}
