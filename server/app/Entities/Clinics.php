<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Clinics extends Model
{
    protected $table = 'clinics';
    public $timestamps = false;

    protected $fillable = [
        'clinic_id',
        'clinic_name',
        'clinic_code',
        'attention_type_id',       
        'clinic_state'
        
    ];
}
