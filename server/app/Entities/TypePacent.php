<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TypePacent extends Model
{
    protected $table = 'type_pacents';

    public $timestamps = false;

    protected $fillable = [
        'type_pacent_id',
        'type_pacent_name',
    ];

}
