<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Specialities extends Model
{
    protected $table = 'specialities';
    public $timestamps = false;

    protected $fillable = [
        'speciality_id',
        'speciality_name',
        'speciality_state'
        
    ];
}
