<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Student;
use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;
use Hash;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $campos = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:students',
            'account_number' => 'required|min:7'

        ]);

        if ($campos->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $campos->errors()
            ], 422);
        }

        //Verificar dominio y subdominio

        $domains = ['unah.hn', 'unah.edu.hn'];

        $domain = explode('@', $request->email)[1];

        if (in_array($domain, $domains)) {
            $user = new Student();
            $user->name = $request->name;
            $user->account_number = $request->account_number;
            $user->email = $request->email;
            $user->password = bcrypt($request->account_number);
            $user->save();
            return response()->json(['status' => 'success', 'message' => 'Usuario registrado con exito'], 200);
        }

        return response()->json([
            'message' => 'Correo electronico no valido'
        ], 422);
    }


    public function login(Request $request)
    {
        $credentials = $request->only('account_number', 'password');

        if ($token = $this->guard()->attempt($credentials)) {

            $student = $this->guard()->user()->where('account_number', $credentials['account_number'])
                ->first();

            if ($student->temporal_password) {
                return response()->json([
                    'temporal_password' => 1,
                    'student_id' => $student->student_id,
                    'token' => $token
                ], 422);
            } else {
                return response()->json([
                    'status' => 'success',
                    'student' => $student,
                    'token' => $token
                ], 200);
            }
        }
        return response()->json(['error' => 'Usuario o contraseña incorrecto'], 401);
    }
    /**
     * Logout User
     */
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    /**
     * Get authenticated user
     */
    public function user()
    {
        $user = User::find($this->guard()->user()->user_id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        try {
            if ($token = $this->guard()->refresh()) {
                return response()
                    ->json(['status' => 'successs'], 200)
                    ->header('Authorization', $token);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['error' => 'refresh_token_error'], 401);
        }
    }
    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard('student');
    }


    public function changePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8'
        ], [
            'password.required' => 'Debe ingresar la contraseña'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'mensaje' => 'Ha ocurrido un error de validación',
                'errors' => $validator->errors()
            ], 422);
        }


        if (!Hash::check($request['password'], $this->guard()->user()->password)) {
            Student::where('student_id', $request['student_id'])->update([
                'password' => bcrypt($request['password']),
                'temporal_password' => 0
            ]);

            return response()->json([
                'mensaje' => 'Se ha cambiado la contraseña correctamente'
            ], 200);
        } else {
            return response()->json([
                'errors' => ['password' => 'La contraseña nueva debe ser diferente a la actual']
            ], 422);
        }
    }
}
