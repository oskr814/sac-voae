<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Auth
Route::prefix('auth')->group(function () {
    // Rutas 'publicas', no requieren autenticacion del usuario

    // Registro
    Route::post('register', 'AuthController@register');

    //Login User
    Route::post('login', 'AuthController@login');
    
    // Refresh the JWT Token
    Route::get('refresh', 'AuthController@refresh');
    
    //cambio de contraseña
    Route::post('recoveyPass', 'AuthController@recoveyPass');

    //Rutas 'privadas' que requieran autenticacion del usuario, se utiliza el middelware auth:api
    Route::middleware('auth:student')->group(function () {
        // Get user info
        Route::get('user', 'AuthController@user');

        // Logout user from application
        Route::post('logout', 'AuthController@logout');

        Route::post('change-password', 'AuthController@changePassword');

       
    });
});
