import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoggedGuard implements CanActivate {
  constructor(private _authService: AuthService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this._authService.authUser()) {
      if (next.routeConfig.path == 'landing') {
        this.router.navigate(['/citas']);
        return false;
      }

      const patient: any = this._authService.getUser();

      if (!patient.temporal_password) {
        this.router.navigate(['/citas']);
        return false;
      }

      return true;
    }

    if (next.routeConfig.path == 'landing') {
      return true;
    }
    
    this.router.navigate(['/landing']);
    return false;
  }
}
