import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private _authService: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const patient: any = this._authService.getUser();

    if (!patient) {
      localStorage.clear();
      this.router.navigate(['/landing']);
      return false;
    }

    if (patient.temporal_password) {
      this.router.navigate(['/newPass']);
      return false;
    }

    return true;
  }
}
