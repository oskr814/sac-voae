import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  test: Date = new Date();
  focus;
  focus1;
  focus2;
  constructor(private authService: AuthService, private router: Router, ) {}

  ngOnInit() {}

  signUpForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      // Validators.pattern('^[A-Za-z]+[a-z0-9._-]+@unah.hn'), "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"
       Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
    ]),
    phone_number: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]{8}'),

    ]),
    dni: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]+$'),
      Validators.minLength(13)
    ]),
    name: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$'),
    ]),
  });

  signUp() {
    if (this.signUpForm.valid) {
      this.authService
        .register(this.signUpForm.value);
    }
  }
}
