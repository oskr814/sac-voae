import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  focus: any;
  focus1: any;
  closeResult: string;

  services = [
    {
      label: 'Programa en Promoción de la Salud',
      services: [
        'Clínica anti-tabaco',
        'Clínica de Atención a la Mujer',
        'Clínica de Estilos de Vida Saludable',
        'Clínica de VIH/SIDA-ITS',
        'Clínica de nutrición',
        'Clínica del Deporte',
        'Terapia Funcional',
      ],
    },
    {
      label: '',
      services: [
        'Atenciones Médicas',
        'Atenciones Odontológicas',
        'Atenciones Psicológicas',
        'Servicio de Laboratorio',
        'Servicio de Farmacia',
        'Ferias de la salud',
        'Charlas en educación en salud a las diferentes carreras',
      ],
    },
  ];

  steps = [
    {
      icon: 'active-40',
      title: 'Regístrate',
      color: 'info',
      Description:
        'Para registrate necesitas ingresar tu DNI, número de cuenta, correo institucional y nombre completo. Recibirás un correo con una contraseña temporal para poder ingresar. Recuerda ingresar al Programa de Atención Integral Estudiantil (PAI-E) <a  target="blank" href="https://pai-voae.unah.edu.hn/login" ><h6 class="text-danger">Clic aquí</h6></a>',
    },
    {
      icon: 'check-bold',
      title: 'Inicia sesión',
      color: 'success',
      Description:
        'Ingresa por primera vez a nuestra plataforma brindando tu número de cuenta y la contraseña que recibiste a través de tu correo electrónico.',
    },
    {
      icon: 'bullet-list-67',
      title: 'Agenda tu cita',
      color: 'default',
      Description:
        'Puedes seleccionar el tipo de atención que necesitas y escoger entre los médicos y horarios disponibles. Agenda tu cita con un día de anticipación al servicio de salud que deseas.',
    },
  ];

  clinicaMujerServices = [
    'Consejería de parejas',
    'Control prenatal',
    'Consejería de planificación familiar',
    'Enseñanza de autoexamen de mama',
    'Examen de citología vaginal',
  ];

  importantDocs = [
    'Carnet estudiantil',
    'Forma 03 impresa',
    'Identificación con foto (si no posees carnet)',
  ];

  constructor(
    private modalService: NgbModal,
    private authService: AuthService
  ) {}

  open(content, type, modalDimension) {
    if (modalDimension === 'sm' && type === 'modal_mini') {
      this.modalService
        .open(content, {
          windowClass: 'modal-mini',
          size: 'sm',
          centered: true,
        })
        .result.then(
          (result) => {
            this.closeResult = 'Closed with: $result';
          },
          (reason) => {
            this.closeResult = 'Dismissed $this.getDismissReason(reason)';
          }
        );
    } else if (modalDimension === '' && type === 'Notification') {
      this.modalService
        .open(content, { windowClass: 'modal-danger', centered: true })
        .result.then(
          (result) => {
            this.closeResult = 'Closed with: $result';
          },
          (reason) => {
            this.closeResult = 'Dismissed $this.getDismissReason(reason)';
          }
        );
    } else {
      this.modalService.open(content, { centered: true }).result.then(
        (result) => {
          this.closeResult = 'Closed with: $result';
        },
        (reason) => {
          this.closeResult = 'Dismissed $this.getDismissReason(reason)';
        }
      );
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return 'with: $reason';
    }
  }

  loginForm = new FormGroup({
    dni: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]{13}'),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9A-Za-zd$@$!%*?&-_].{7,}'),
    ]),
  });

  forgotPassForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('[A-Za-z]+[a-z0-9._-]+@unah.hn'),
    ]),
    account_number: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]+$'),
    ]),
  });

  ngOnInit() {}

  login() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value);
    }
  }
}
