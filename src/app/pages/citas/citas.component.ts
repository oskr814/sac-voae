import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppointmentService } from '../../services/appointment.service';
import { SnotifyService, SnotifyPosition } from 'ng-snotify';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-citas',
  templateUrl: './citas.component.html',
  styleUrls: ['./citas.component.scss'],
})
export class CitasComponent implements OnInit {
  clinicas: any[] = ['Clinica 1', 'Clinica 2', 'Clinica 3'];
  tipoAtencion: any[] = ['Tipo 1', 'Tipo 2', 'Tipo 3'];
  medicos: any[] = ['Medico 1', 'Medico 2', 'Medico 3'];
  horarioMedico = [
    { label: '8:00 AM', schedule_id: 800, disabled: null },
    { label: '8:30 AM', schedule_id: 850, disabled: null },
    { label: '9:00 AM', schedule_id: 900, disabled: null },
    { label: '9:30 AM', schedule_id: 950, disabled: null },
    { label: '10:00 AM', schedule_id: 1000, disabled: null },
    { label: '10:30 AM', schedule_id: 1050, disabled: null },
    { label: '11:00 AM', schedule_id: 1100, disabled: null },
    { label: '11:30 AM', schedule_id: 1150, disabled: null },
    { label: '12:00 PM', schedule_id: 1200, disabled: null },
    { label: '12:30 PM', schedule_id: 1250, disabled: null },
    { label: '1:00 PM', schedule_id: 1300, disabled: null },
    { label: '1:30 PM', schedule_id: 1350, disabled: null },
    { label: '2:00 PM', schedule_id: 1400, disabled: null },
    { label: '2:30 PM', schedule_id: 1450, disabled: null },
    { label: '3:00 PM', schedule_id: 1500, disabled: null },
    { label: '3:30 PM', schedule_id: 1550, disabled: null },
    { label: '4:00 PM', schedule_id: 1600, disabled: null },
    { label: '4:30 PM', schedule_id: 1650, disabled: null },
    { label: '5:00 PM', schedule_id: 1700, disabled: null },
    { label: '5:30 PM', schedule_id: 1750, disabled: null },
    { label: '6:00 PM', schedule_id: 1800, disabled: null },
    { label: '6:30 PM', schedule_id: 1850, disabled: null },
    { label: '7:00 PM', schedule_id: 1900, disabled: null },
  ];
  snotifyConfig = {
    timeout: 5000,
    pauseOnHover: true,
    showProgressBar: false,
  };
  markDisabled;
  todayDate;
  maxDate;
  cita;
  loading = true;

  constructor(
    private appointmentService: AppointmentService,
    private snotifyService: SnotifyService,
    private calendar: NgbCalendar,
    private router: Router
  ) {
    //Obteniendo data inicial
    this.appointmentService
      .getData({
        patient_id: JSON.parse(localStorage.getItem('patient')).patient_id,
      })
      .subscribe((data: any) => {
        console.log(data);
        this.cita = data.cita_pendiente;
        this.tipoAtencion = data.attention_types;
        this.loading = false;
      });

    //Deshabilitar los campos del form
    this.citasForm.get('medico').disable();
    this.citasForm.get('clinica').disable();
    this.citasForm.get('fecha').disable();

    //Establecer rango de fechas
    this.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 6;
    const current = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
    this.todayDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate(),
    };
    const maxDate = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);
    this.maxDate = {
      year: maxDate.getFullYear(),
      month: maxDate.getMonth() + 1,
      day: maxDate.getDate(),
    };
  }

  ngOnInit(): void {}

  citasForm = new FormGroup({
    tipoAtencion: new FormControl({ value: '', disabled: false }, [
      Validators.required,
    ]),
    clinica: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    medico: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    fecha: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
    horarios: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
  });

  setClinic() {
    // console.log(this.citasForm.get('tipoAtencion').value);
    this.clinicas = this.tipoAtencion.find(
      (tipo) =>
        tipo.attention_type_id ===
        parseInt(this.citasForm.get('tipoAtencion').value)
    ).clinics;
    // console.log(this.clinicas);
    this.citasForm.get('clinica').enable();
    this.citasForm.get('medico').disable();
    this.citasForm.get('fecha').disable();
    this.citasForm.get('horarios').disable();

    this.citasForm.get('clinica').setValue('');
    this.citasForm.get('medico').setValue('');
    this.citasForm.get('fecha').setValue('');
    this.citasForm.get('horarios').setValue('');
  }

  setDoctors() {
    this.medicos = this.clinicas.find(
      (clinica) =>
        clinica.clinic_id === parseInt(this.citasForm.get('clinica').value)
    ).doctors;
    // console.log(this.medicos);
    this.citasForm.get('medico').enable();
    this.citasForm.get('fecha').disable();
    this.citasForm.get('horarios').disable();
    this.citasForm.get('medico').setValue('');
    this.citasForm.get('fecha').setValue('');
    this.citasForm.get('horarios').setValue('');
  }

  enableCalendar() {
    // console.log(this.medicos);
    this.citasForm.get('fecha').enable();
    this.citasForm.get('horarios').disable();
    this.citasForm.get('fecha').setValue('');
    this.citasForm.get('horarios').setValue('');
  }

  changeCalendar() {
    //console.log('calendario cambió');
    // console.log(this.citasForm.value);
    let data = {
      tipoAtencion: this.citasForm.get('tipoAtencion').value,
      medico: this.citasForm.get('medico').value,
      clinica: this.citasForm.get('clinica').value,
      fecha: this.citasForm.get('fecha').value,
      patient_id: JSON.parse(localStorage.getItem('patient')).patient_id,
    };
    //console.log(data);

    this.appointmentService.getHorarios(data).subscribe((res: any) => {
      console.log(res);
      this.horarioMedico = res.horario;
      this.citasForm.get('horarios').enable();
      this.citasForm.get('horarios').setValue('');
    });
  }

  agendarCita() {
    //console.log(this.citasForm.errors);
    //console.log(this.citasForm.value);
    if (this.citasForm.valid) {
      let data = {
        tipoAtencion: this.citasForm.get('tipoAtencion').value,
        medico: this.citasForm.get('medico').value,
        clinica: this.citasForm.get('clinica').value,
        fecha: this.citasForm.get('fecha').value,
        horarios: this.citasForm.get('horarios').value,
        patient_id: JSON.parse(localStorage.getItem('patient')).patient_id,
      };
      this.appointmentService.createAppointment(data).subscribe(
        (data: any) => {
          console.log(data);
          if (data.cita) {
            this.cita = data.cita;
            this.snotifyService.success(data.mensaje, '', this.snotifyConfig);
            this.citasForm.disable();
            this.citasForm.setValue({
              tipoAtencion: '',
              medico: '',
              clinica: '',
              fecha: '',
              horarios: '',
            });
          }
        },
        (err) => {
          console.log(err);
          this.snotifyService.error(
            'La cita no pudo ser registrada',
            '',
            this.snotifyConfig
          );
        }
      );
    }
  }

  cancelarCita() {
    this.snotifyService.confirm(
      '¿Seguro que desea cancelar la cita?. Esta acción no se puede revertir',
      '',
      {
        timeout: 7000,
        showProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true,
        buttons: [
          {
            text: 'Sí',
            action: (toast) => {
              console.log('Clicked: Yes');
              this.appointmentService
                .cancelAppointment({ cita: this.cita.ticket_id })
                .subscribe(
                  (response: any) => {
                    console.log(response);
                    this.cita = null;
                    this.citasForm.get('tipoAtencion').enable();
                    this.snotifyService.success(
                      response.mensaje,
                      '',
                      this.snotifyConfig
                    );
                    this.snotifyService.remove(toast.id);
                  },
                  (err) => {
                    console.log(err);
                    this.snotifyService.success(
                      'La cita no pudo ser cancelada.',
                      '',
                      this.snotifyConfig
                    );
                  }
                );
            },
            bold: false,
          },
          {
            text: 'No',
            action: (toast) => {
              //console.log('Clicked: No');
              this.snotifyService.remove(toast.id);
            },
          },
        ],
      }
    );
  }

  quoteServices(){
    this.router.navigate(['/quoteServices'])
  }
}
