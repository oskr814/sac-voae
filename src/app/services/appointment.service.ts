import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient) { }

  getData( userInfo:any ){
    return this.http.post( environment.baseUrl + '/general/get-data', userInfo );
  }

  getHorarios( info:any ){
    return this.http.post( environment.baseUrl + '/general/get-horario', info );
  }

  cancelAppointment(id){
    return this.http.post(environment.baseUrl + '/general/cancel-appointment', id)
  }

  createAppointment( appointmentInfo:any ){
    return this.http.post(environment.baseUrl + '/general/create-appointment', appointmentInfo);
  }

}
