import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { SnotifyService } from 'ng-snotify';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  snotifyConfig = {
    timeout: 5000,
    pauseOnHover: true,
    showProgressBar: false,
  };

  $user = new BehaviorSubject<boolean>(this.authUser());
  constructor(
    private http: HttpClient,
    private router: Router,
    private snotifyService: SnotifyService
  ) {}

  register(user: any) {
    this.http
      .post(`${environment.baseUrl}/auth/register`, user)
      .toPromise()
      .then((res: any) => {
          this.snotifyService.success(res.mensaje, '', this.snotifyConfig);
          this.router.navigate(['/newPass']);
      })
      .catch((response) => { 
        console.log(response);
        
        for (const error in response.error.validations) {
          console.log(response.error.validations[error][0]);
          this.snotifyService.error(response.error.validations[error][0], '', this.snotifyConfig);
        }

        if (response.error.mensaje) {
          this.snotifyService.error(response.error.mensaje, '', this.snotifyConfig);
        }

      });
  }

  authUser() {
    return !!localStorage.getItem('token');
  }

  login(credentials: any) {
    this.http
      .post(`${environment.baseUrl}/auth/login`, credentials)
      .toPromise()
      .then((response: any) => {
        console.log(response);
        
        localStorage.setItem('token', response.token);
        localStorage.setItem(
          'patient',
          JSON.stringify({
            patient_id: response.patient.patient_id,
          })
        );

        this.$user.next(true);

        this.router.navigate(['/citas']);
      })
      .catch((response) => {
        if (response.error.temporal_password) {
          // console.log(response);
          
          localStorage.setItem('token', response.error.token);
          localStorage.setItem(
            'patient',
            JSON.stringify({ patient_id: response.error.patient_id, temporal_password: 1 })
          );
          this.router.navigate(['/newPass']);
        }
        
        this.snotifyService.error(response.error.mensaje, '', this.snotifyConfig);
      });
  }

  logOut() {
    localStorage.clear();

    this.router.navigate(['/landing']);

    this.snotifyService.success(
      'Sesión cerrada con exito!',
      '',
      this.snotifyConfig
    );

    this.$user.next(false);
  }

  changePassword(data) {
    const patient = this.getUser();

    if (patient) {
      const credentials = {
        password: data.password,
        tempPassword: data.tempPass,
        patient_id: patient.patient_id,
        token: localStorage.getItem('token'),
      };

      this.http
        .post(`${environment.baseUrl}/auth/change-password`, credentials)
        .toPromise()
        .then((response: any) => {
          const patient = JSON.parse(localStorage.getItem('patient'));

          patient.temporal_password = 0;

          localStorage.setItem('patient', JSON.stringify(patient));

          this.snotifyService.success(response.mensaje, '', this.snotifyConfig);

          this.router.navigate(['/citas']);
        }).catch(response => {
          this.snotifyService.error(response.error.mensaje, '', this.snotifyConfig);
        });
    }
  }

  getUser(): any {
    return JSON.parse(localStorage.getItem('patient'));
  }

  recoveryPassword(data){
    this.http.post(`${environment.baseUrl}/auth/recoveyPass`, data).toPromise().then(
      (res:any) => {
        //console.log(res)
        this.snotifyService.success(res.message, '', this.snotifyConfig);
        this.router.navigate(['/landing']);
      }
    ).catch(
      (err:any) =>{ //console.log(err)
      this.snotifyService.error(err.error.message, '', this.snotifyConfig);
    }
    )
  }

}
