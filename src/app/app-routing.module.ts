import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './pages/signup/signup.component';
import { LandingComponent } from './pages/landing/landing.component';
import { NewPassComponent } from './pages/new-pass/new-pass.component';
import { CitasComponent } from './pages/citas/citas.component';
import { AuthGuard } from './guards/auth.guard';
import { LoggedGuard } from './guards/logged.guard';
import {RecoveryPassComponent} from './pages/recovery-pass/recovery-pass.component'
import { QuoteServicesComponent } from './pages/quote-services/quote-services.component';

const routes: Routes = [
  { path: 'register', component: SignupComponent },
  { path: 'landing', component: LandingComponent, canActivate: [LoggedGuard] },
  { path: 'citas', component: CitasComponent, canActivate: [AuthGuard] },
  { path: 'newPass', component: NewPassComponent, canActivate: [LoggedGuard] },
  { path: 'recoveryPass', component: RecoveryPassComponent },
  { path: 'quoteServices', component: QuoteServicesComponent },
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
